# Task overview

You are viewing / updating user setting as three different users with different user permissions which you can switch between the user menu on the top left menu.
Get the initial form data and other field data that you will need from the data folder. After the initial data is loaded and all fields are populated the user can change the values in the form. When the form is saved, console log the data in the same format and data types as you initially got it. If the form was modified and not saved warn the user before navigating / switching users (take note that each user has its own route).

## Task files
* **./design/** – design files and UI map.
* **./fonts/** – font used in the design.
* **./README.md** – instructions
* **./data folder** – data (JSON)
* **./permission folder** – roles for each user (JSON)

## Switching users
When switching users reset the form data to the initial values. Compare current roles to the user permissions to get data you can load in the roles dropdowns and whether you can remove the currently selected ones.

## Roles & role rules
* **Account admins** – can modify anything in divisions and teams.
They can assign/unassign division admin, team admin, read only user and user roles to other users.

* **Division admins** – can modify anything in divisions and teams that belong to those divisions. They can assign/unassign division admin, team admin, read only user and user roles to other users.

* **Team admins** – can modify anything in teams.
They can assign/unassign team admin and user roles to other users.

* **Users** – can modify personal information and settings.
They can only see which teams they are assigned to, but they can’t modify their roles.

## Project details
1.	Create a react (TypeScript) project
2.	Setup ESLint, StyleLint, Prettier, Husky, and LESS support to the project
3.	Setup script(s) for ESLint, StyleLint, Prettier to run on pre-commit
4.	Install MobX (reactive state), React Router 6.0+ and React Suite (UI library)
5.	Create/extend styles for the UI elements from the UI map (Using custom LESS files)
6.	Create routes for each user type (BASE_URL/:currentUserID e.g. http://localhost:3000/1)
7.	Create a MobX store for the current user permissions data.
8.	Create the layout (responsive) from the design Adobe XD files.
9.	Implement logic and complete the task.

## Bonus points
1.	UI animations
2.	Code comments, components usage examples and structure
3.	MobX store data persistence using local storage (for each user type)
4.	Pixel perfect UI
5.	Anything else you feel you could add.
